#ifndef CONFIG_H
#define CONFIG_H

/*
 * On aarch64 it depends on the kernel configuration. Can be discovered at
 * runtime though but what a mess
 */
#define ULINUX_PAGE_SHIFT 12 

/* IPV4: without this, the code paths do default to IPv6 */
#define CONFIG_IPV4 1

#ifndef CONFIG_IPV4
	/* any not defined byte of the IPv6 address will default to 0 */
	/* IN6ADDR_ANY is all 0 */
	/* #define CONFIG_LISTENING_IPV6_BYTE_0 0x00
	#define CONFIG_LISTENING_IPV6_BYTE_1 0x00
	#define CONFIG_LISTENING_IPV6_BYTE_2 0x00
	#define CONFIG_LISTENING_IPV6_BYTE_3 0x00
	#define CONFIG_LISTENING_IPV6_BYTE_4 0x00
	#define CONFIG_LISTENING_IPV6_BYTE_5 0x00
	#define CONFIG_LISTENING_IPV6_BYTE_6 0x00
	#define CONFIG_LISTENING_IPV6_BYTE_7 0x00
	#define CONFIG_LISTENING_IPV6_BYTE_8 0x00
	#define CONFIG_LISTENING_IPV6_BYTE_9 0x00
	#define CONFIG_LISTENING_IPV6_BYTE_A 0x00
	#define CONFIG_LISTENING_IPV6_BYTE_B 0x00
	#define CONFIG_LISTENING_IPV6_BYTE_C 0x00
	#define CONFIG_LISTENING_IPV6_BYTE_D 0x00
	#define CONFIG_LISTENING_IPV6_BYTE_E 0x00
	#define CONFIG_LISTENING_IPV6_BYTE_F 0x00 */
#else /* IPv4 */
	/* if not defined, LISTENING_IPV4 will default to INADDR_ANY */
	/* #define CONFIG_LISTENING_IPV4 0xaabbccdd */
#endif

/*
 * 16 bits value for the port (below 1024, must be root, that you must be for
 * chroot anyway)
 */
#define CONFIG_LISTENING_PORT 80

/* the chroot patch used upon start */
#define CONFIG_CHROOT_PATH "/var/www"

/* time out for a socket read/write, in seconds. 4 secs is huge */
#define CONFIG_CNX_WAIT_TIMEOUT 4

/* default file */
#define CONFIG_DEFAULT_FILE (u8*)"/index.html"
#define CONFIG_DEFAULT_FILE_MIME (u8*)"/index.html.mime"

#endif
