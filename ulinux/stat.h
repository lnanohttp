#ifndef ULINUX_STAT_H
#define ULINUX_STAT_H
/*
 * this code is protected by the GNU affero GPLv3
 * author:Sylvain BERTRAND
 */

/*
 * for the "old" stat syscall which could be arch specific, should use the
 * "new" statx which is "arch independant"
 */
#include <ulinux/arch/stat.h>

#define ULINUX_AT_CWD -100

/*----------------------------------------------------------------------------*/
#define ULINUX_S_IFMT	00170000
#define ULINUX_S_IFSOCK  0140000
#define ULINUX_S_IFLNK   0120000
#define ULINUX_S_IFREG   0100000
#define ULINUX_S_IFBLK   0060000
#define ULINUX_S_IFDIR   0040000
#define ULINUX_S_IFCHR   0020000
#define ULINUX_S_IFIFO   0010000

#define ULINUX_S_ISUID   0004000
#define ULINUX_S_ISGID   0002000
#define ULINUX_S_ISVTX   0001000

#define ULINUX_S_IRWXU	   00700
#define ULINUX_S_IRUSR	   00400
#define ULINUX_S_IWUSR	   00200
#define ULINUX_S_IXUSR	   00100

#define ULINUX_S_IRWXG	   00070
#define ULINUX_S_IRGRP	   00040
#define ULINUX_S_IWGRP	   00020
#define ULINUX_S_IXGRP	   00010

#define ULINUX_S_IRWXO	   00007
#define ULINUX_S_IROTH	   00004
#define ULINUX_S_IWOTH	   00002
#define ULINUX_S_IXOTH	   00001
/*----------------------------------------------------------------------------*/

#define ULINUX_S_IRWXUGO (ULINUX_S_IRWXU|ULINUX_S_IRWXG|ULINUX_S_IRWXO)
#define ULINUX_S_IALLUGO (ULINUX_S_ISUID|ULINUX_S_ISGID|ULINUX_S_ISVTX\
\	|ULINUX_S_IRWXUGO)
#define ULINUX_S_IRUGO   (ULINUX_S_IRUSR|ULINUX_S_IRGRP|ULINUX_S_IROTH)
#define ULINUX_S_IWUGO   (ULINUX_S_IWUSR|ULINUX_S_IWGRP|ULINUX_S_IWOTH)
#define ULINUX_S_IXUGO   (ULINUX_S_IXUSR|ULINUX_S_IXGRP|ULINUX_S_IXOTH)

/*============================================================================*/
/* statx syscall to prefer than stat syscall on not too old kernels */

/*
 * Timestamp structure for the timestamps in struct statx.
 *
 * tv_sec holds the number of seconds before (negative) or after (positive)
 * 00:00:00 1st January 1970 UTC.
 *
 * tv_nsec holds a number of nanoseconds (0..999,999,999) after the tv_sec time.
 *
 * __reserved is held in case we need a yet finer resolution.
 */
struct ulinux_statx_timestamp {
	ulinux_s64	tv_sec;
	ulinux_u32	tv_nsec;
	ulinux_s32	__reserved;
};

/*
 * Structures for the extended file attribute retrieval system call
 * (statx()).
 *
 * The caller passes a mask of what they're specifically interested in as a
 * parameter to statx().  What statx() actually got will be indicated in
 * st_mask upon return.
 *
 * For each bit in the mask argument:
 *
 * - if the datum is not supported:
 *
 *   - the bit will be cleared, and
 *
 *   - the datum will be set to an appropriate fabricated value if one is
 *     available (eg. CIFS can take a default uid and gid), otherwise
 *
 *   - the field will be cleared;
 *
 * - otherwise, if explicitly requested:
 *
 *   - the datum will be synchronised to the server if AT_STATX_FORCE_SYNC is
 *     set or if the datum is considered out of date, and
 *
 *   - the field will be filled in and the bit will be set;
 *
 * - otherwise, if not requested, but available in approximate form without any
 *   effort, it will be filled in anyway, and the bit will be set upon return
 *   (it might not be up to date, however, and no attempt will be made to
 *   synchronise the internal state first);
 *
 * - otherwise the field and the bit will be cleared before returning.
 *
 * Items in STATX_BASIC_STATS may be marked unavailable on return, but they
 * will have values installed for compatibility purposes so that stat() and
 * co. can be emulated in userspace.
 */
struct ulinux_statx {
	/* 0x00 */
	ulinux_u32 mask;	/* What results were written [uncond] */
	ulinux_u32 blksize;	/* Preferred general I/O size [uncond] */
	ulinux_u64 attributes;	/* Flags conveying information about the file [uncond] */
	/* 0x10 */
	ulinux_u32 nlink;	/* Number of hard links */
	ulinux_u32 uid;	/* User ID of owner */
	ulinux_u32 gid;	/* Group ID of owner */
	ulinux_u16 mode;	/* File mode */
	ulinux_u16 __spare0[1];
	/* 0x20 */
	ulinux_u64 ino;	/* Inode number */
	ulinux_u64 size;	/* File size */
	ulinux_u64 blocks;	/* Number of 512-byte blocks allocated */
	ulinux_u64 attributes_mask; /* Mask to show what's supported in stx_attributes */
	/* 0x40 */
	struct ulinux_statx_timestamp atime;	/* Last access time */
	struct ulinux_statx_timestamp btime;	/* File creation time */
	struct ulinux_statx_timestamp ctime;	/* Last attribute change time */
	struct ulinux_statx_timestamp mtime;	/* Last data modification time */
	/* 0x80 */
	ulinux_u32 rdev_major;	/* Device ID of special file [if bdev/cdev] */
	ulinux_u32 rdev_minor;
	ulinux_u32 dev_major;	/* ID of device containing file [uncond] */
	ulinux_u32 dev_minor;
	/* 0x90 */
	ulinux_u64 __spare2[14];	/* Spare space for future expansion */
	/* 0x100 */
};

/*
 * Flags to be stx_mask
 *
 * Query request/result mask for statx() and struct statx::stx_mask.
 *
 * These bits should be set in the mask argument of statx() to request
 * particular items when calling statx().
 */
#define ULINUX_STATX_TYPE		0x00000001U	/* Want/got stx_mode & S_IFMT */
#define ULINUX_STATX_MODE		0x00000002U	/* Want/got stx_mode & ~S_IFMT */
#define ULINUX_STATX_NLINK		0x00000004U	/* Want/got stx_nlink */
#define ULINUX_STATX_UID		0x00000008U	/* Want/got stx_uid */
#define ULINUX_STATX_GID		0x00000010U	/* Want/got stx_gid */
#define ULINUX_STATX_ATIME		0x00000020U	/* Want/got stx_atime */
#define ULINUX_STATX_MTIME		0x00000040U	/* Want/got stx_mtime */
#define ULINUX_STATX_CTIME		0x00000080U	/* Want/got stx_ctime */
#define ULINUX_STATX_INO		0x00000100U	/* Want/got stx_ino */
#define ULINUX_STATX_SIZE		0x00000200U	/* Want/got stx_size */
#define ULINUX_STATX_BLOCKS		0x00000400U	/* Want/got stx_blocks */
#define ULINUX_STATX_BASIC_STATS	0x000007ffU	/* The stuff in the normal stat struct */
#define ULINUX_STATX_BTIME		0x00000800U	/* Want/got stx_btime */
#define ULINUX_STATX_ALL		0x00000fffU	/* All currently supported flags */
#define ULINUX_STATX__RESERVED		0x80000000U	/* Reserved for future struct statx expansion */

/*
 * Attributes to be found in stx_attributes and masked in stx_attributes_mask.
 *
 * These give information about the features or the state of a file that might
 * be of use to ordinary userspace programs such as GUIs or ls rather than
 * specialised tools.
 *
 * Note that the flags marked [I] correspond to generic FS_IOC_FLAGS
 * semantically.  Where possible, the numerical value is picked to correspond
 * also.
 */
#define ULINUX_STATX_ATTR_COMPRESSED	0x00000004 /* [I] File is compressed by the fs */
#define ULINUX_STATX_ATTR_IMMUTABLE	0x00000010 /* [I] File is marked immutable */
#define ULINUX_STATX_ATTR_APPEND	0x00000020 /* [I] File is append-only */
#define ULINUX_STATX_ATTR_NODUMP	0x00000040 /* [I] File is not to be dumped */
#define ULINUX_STATX_ATTR_ENCRYPTED	0x00000800 /* [I] File requires key to decrypt in fs */

#define ULINUX_STATX_ATTR_AUTOMOUNT	0x00001000 /* Dir: Automount trigger */
#endif
