#ifndef ULINUX_ARCH_SYSC_H
#define ULINUX_ARCH_SYSC_H
/*
 * this code is protected by the GNU affero GPLv3
 * author:Sylvain BERTRAND
 */

/*
 * linux takes system call args in registers:
 *	syscall number	x8
 *	arg 1		x0
 *	arg 2		x1
 *	arg 3		x2
 *	arg 4		x3
 *	arg 5		x4
 *	arg 6		x5
 *	arg 7		x6
 *
 * The compiler is going to form a call by coming here, through PSEUDO, with
 * arguments
 *	syscall number	in the DO_CALL macro
 *	arg 1		x0
 *	arg 2		x1
 *	arg 3		x2
 *	arg 4		x3
 *	arg 5		x4
 *	arg 6		x5
 *	arg 7		x6
 *
 */

/*
 * List of system calls which are supported as vsyscalls:
 *     CLOCK_GETRES
 *     CLOCK_GETTIME
 *     GETTIMEOFDAY
 */

/*============================================================================*/
#ifdef __GNUC__

/* gnu libc derived */

#define ulinux_sysc(name, nr, args...)						\
	({									\
		LOAD_ARGS_##nr(args)						\
		register unsigned long _x8 asm ("x8") = (__ULINUX_NR_##name);	\
		asm volatile (							\
		"svc 0"								\
		:"=r"(_x0)							\
		:"r"(_x8) ASM_ARGS_##nr						\
		: "memory");							\
		(long)_x0;})


/*----------------------------------------------------------------------------*/
/* x0 is input/output: arg0 as input, sysc return value as output */
#define LOAD_ARGS_0()				\
	register long _x0 asm ("x0");

#define LOAD_ARGS_1(x0)				\
	long _x0tmp = (long)(x0);		\
	LOAD_ARGS_0()				\
	_x0 = _x0tmp;

#define LOAD_ARGS_2(x0, x1)			\
	long _x1tmp = (long)(x1);		\
	LOAD_ARGS_1(x0)				\
	register long _x1 asm ("x1") = _x1tmp;

#define LOAD_ARGS_3(x0, x1, x2)			\
	long _x2tmp = (long) (x2);		\
	LOAD_ARGS_2(x0, x1)			\
	register long _x2 asm ("x2") = _x2tmp;

#define LOAD_ARGS_4(x0, x1, x2, x3)		\
	long _x3tmp = (long)(x3);		\
	LOAD_ARGS_3(x0, x1, x2)			\
	register long _x3 asm ("x3") = _x3tmp;

#define LOAD_ARGS_5(x0, x1, x2, x3, x4)		\
	long _x4tmp = (long)(x4);		\
	LOAD_ARGS_4(x0, x1, x2, x3)		\
	register long _x4 asm ("x4") = _x4tmp;

#define LOAD_ARGS_6(x0, x1, x2, x3, x4, x5)	\
	long _x5tmp = (long)(x5);		\
	LOAD_ARGS_5 (x0, x1, x2, x3, x4)	\
	register long _x5 asm ("x5") = _x5tmp;

#define LOAD_ARGS_7(x0, x1, x2, x3, x4, x5, x6)	\
	long _x6tmp = (long)(x6);		\
	LOAD_ARGS_6 (x0, x1, x2, x3, x4, x5)	\
	register long _x6 asm ("x6") = _x6tmp;
/*----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------*/
#define ASM_ARGS_0
#define ASM_ARGS_1	, "r"(_x0)
#define ASM_ARGS_2	ASM_ARGS_1, "r"(_x1)
#define ASM_ARGS_3	ASM_ARGS_2, "r"(_x2)
#define ASM_ARGS_4	ASM_ARGS_3, "r"(_x3)
#define ASM_ARGS_5	ASM_ARGS_4, "r"(_x4)
#define ASM_ARGS_6	ASM_ARGS_5, "r"(_x5)
#define ASM_ARGS_7	ASM_ARGS_6, "r"(_x6)
/*----------------------------------------------------------------------------*/
#else
#error "no aarch64 syscall code is defined for your toolchain"
#endif /* __GNUC__ */

/*============================================================================*/
/*
 * this list is probably not up to date, see:
 *   - do generate the linux headers for arm64 arch, and follow linux/unistd.h
 */

#define __ULINUX_NR_io_setup 0
#define __ULINUX_NR_io_destroy 1
#define __ULINUX_NR_io_submit 2
#define __ULINUX_NR_io_cancel 3
#define __ULINUX_NR_io_getevents 4

/* fs/xattr.c */
#define __ULINUX_NR_setxattr 5
#define __ULINUX_NR_lsetxattr 6
#define __ULINUX_NR_fsetxattr 7
#define __ULINUX_NR_getxattr 8
#define __ULINUX_NR_lgetxattr 9
#define __ULINUX_NR_fgetxattr 10
#define __ULINUX_NR_listxattr 11
#define __ULINUX_NR_llistxattr 12
#define __ULINUX_NR_flistxattr 13
#define __ULINUX_NR_removexattr 14
#define __ULINUX_NR_lremovexattr 15
#define __ULINUX_NR_fremovexattr 16

/* fs/dcache.c */
#define __ULINUX_NR_getcwd 17

/* fs/cookies.c */
#define __ULINUX_NR_lookup_dcookie 18

/* fs/eventfd.c */
#define __ULINUX_NR_eventfd2 19

/* fs/eventpoll.c */
#define __ULINUX_NR_epoll_create1 20
#define __ULINUX_NR_epoll_ctl 21
#define __ULINUX_NR_epoll_pwait 22

/* fs/fcntl.c */
#define __ULINUX_NR_dup 23
#define __ULINUX_NR_dup3 24
#define __ULINUX_NR_fcntl 25

/* fs/inotify_user.c */
#define __ULINUX_NR_inotify_init1 26
#define __ULINUX_NR_inotify_add_watch 27
#define __ULINUX_NR_inotify_rm_watch 28

/* fs/ioctl.c */
#define __ULINUX_NR_ioctl 29

/* fs/ioprio.c */
#define __ULINUX_NR_ioprio_set 30
#define __ULINUX_NR_ioprio_get 31

/* fs/locks.c */
#define __ULINUX_NR_flock 32

/* fs/namei.c */
#define __ULINUX_NR_mknodat 33
#define __ULINUX_NR_mkdirat 34
#define __ULINUX_NR_unlinkat 35
#define __ULINUX_NR_symlinkat 36
#define __ULINUX_NR_linkat 37
/* renameat is superseded with flags by renameat2 */
#define __ULINUX_NR_renameat 38

/* fs/namespace.c */
#define __ULINUX_NR_umount2 39
#define __ULINUX_NR_mount 40
#define __ULINUX_NR_pivot_root 41

/* fs/nfsctl.c */
#define __ULINUX_NR_nfsservctl 42

/* fs/open.c */
#define __ULINUX_NR_statfs 43
#define __ULINUX_NR_fstatfs 44
#define __ULINUX_NR_truncate 45
#define __ULINUX_NR_ftruncate 46

#define __ULINUX_NR_fallocate 47
#define __ULINUX_NR_faccessat 48
#define __ULINUX_NR_chdir 49
#define __ULINUX_NR_fchdir 50
#define __ULINUX_NR_chroot 51
#define __ULINUX_NR_fchmod 52
#define __ULINUX_NR_fchmodat 53
#define __ULINUX_NR_fchownat 54
#define __ULINUX_NR_fchown 55
#define __ULINUX_NR_openat 56
#define __ULINUX_NR_close 57
#define __ULINUX_NR_vhangup 58

/* fs/pipe.c */
#define __ULINUX_NR_pipe2 59

/* fs/quota.c */
#define __ULINUX_NR_quotactl 60

/* fs/readdir.c */
#define __ULINUX_NR_getdents64 61

/* fs/read_write.c */
#define __ULINUX_NR_lseek 62
#define __ULINUX_NR_read 63
#define __ULINUX_NR_write 64
#define __ULINUX_NR_readv 65
#define __ULINUX_NR_writev 66
#define __ULINUX_NR_pread64 67
#define __ULINUX_NR_pwrite64 68
#define __ULINUX_NR_preadv 69
#define __ULINUX_NR_pwritev 70

/* fs/sendfile.c */
#define __ULINUX_NR_sendfile 71

/* fs/select.c */
#define __ULINUX_NR_pselect6 72
#define __ULINUX_NR_ppoll 73

/* fs/signalfd.c */
#define __ULINUX_NR_signalfd4 74

/* fs/splice.c */
#define __ULINUX_NR_vmsplice 75
#define __ULINUX_NR_splice 76
#define __ULINUX_NR_tee 77

/* fs/stat.c */
#define __ULINUX_NR_readlinkat 78
#define __ULINUX_NR_fstatat 79
#define __ULINUX_NR_fstat 80

/* fs/sync.c */
#define __ULINUX_NR_sync 81
#define __ULINUX_NR_fsync 82
#define __ULINUX_NR_fdatasync 83
#define __ULINUX_NR_sync_file_range 84

/* fs/timerfd.c */
#define __ULINUX_NR_timerfd_create 85
#define __ULINUX_NR_timerfd_settime 86
#define __ULINUX_NR_timerfd_gettime 87

/* fs/utimes.c */
#define __ULINUX_NR_utimensat 88

/* kernel/acct.c */
#define __ULINUX_NR_acct 89

/* kernel/capability.c */
#define __ULINUX_NR_capget 90
#define __ULINUX_NR_capset 91

/* kernel/exec_domain.c */
#define __ULINUX_NR_personality 92

/* kernel/exit.c */
#define __ULINUX_NR_exit 93
#define __ULINUX_NR_exit_group 94
#define __ULINUX_NR_waitid 95

/* kernel/fork.c */
#define __ULINUX_NR_set_tid_address 96
#define __ULINUX_NR_unshare 97

/* kernel/futex.c */
#define __ULINUX_NR_futex 98
#define __ULINUX_NR_set_robust_list 99
#define __ULINUX_NR_get_robust_list 100

/* kernel/hrtimer.c */
#define __ULINUX_NR_nanosleep 101

/* kernel/itimer.c */
#define __ULINUX_NR_getitimer 102
#define __ULINUX_NR_setitimer 103

/* kernel/kexec.c */
#define __ULINUX_NR_kexec_load 104

/* kernel/module.c */
#define __ULINUX_NR_init_module 105
#define __ULINUX_NR_delete_module 106

/* kernel/posix-timers.c */
#define __ULINUX_NR_timer_create 107
#define __ULINUX_NR_timer_gettime 108
#define __ULINUX_NR_timer_getoverrun 109
#define __ULINUX_NR_timer_settime 110
#define __ULINUX_NR_timer_delete 111
#define __ULINUX_NR_clock_settime 112
#define __ULINUX_NR_clock_gettime 113
#define __ULINUX_NR_clock_getres 114
#define __ULINUX_NR_clock_nanosleep 115

/* kernel/printk.c */
#define __ULINUX_NR_syslog 116

/* kernel/ptrace.c */
#define __ULINUX_NR_ptrace 117

/* kernel/sched/core.c */
#define __ULINUX_NR_sched_setparam 118
#define __ULINUX_NR_sched_setscheduler 119
#define __ULINUX_NR_sched_getscheduler 120
#define __ULINUX_NR_sched_getparam 121
#define __ULINUX_NR_sched_setaffinity 122
#define __ULINUX_NR_sched_getaffinity 123
#define __ULINUX_NR_sched_yield 124
#define __ULINUX_NR_sched_get_priority_max 125
#define __ULINUX_NR_sched_get_priority_min 126
#define __ULINUX_NR_sched_rr_get_interval 127

/* kernel/signal.c */
#define __ULINUX_NR_restart_syscall 128
#define __ULINUX_NR_kill 129
#define __ULINUX_NR_tkill 130
#define __ULINUX_NR_tgkill 131
#define __ULINUX_NR_sigaltstack 132
#define __ULINUX_NR_rt_sigsuspend 133
#define __ULINUX_NR_rt_sigaction 134
#define __ULINUX_NR_rt_sigprocmask 135
#define __ULINUX_NR_rt_sigpending 136
#define __ULINUX_NR_rt_sigtimedwait 137
#define __ULINUX_NR_rt_sigqueueinfo 138
#define __ULINUX_NR_rt_sigreturn 139

/* kernel/sys.c */
#define __ULINUX_NR_setpriority 140
#define __ULINUX_NR_getpriority 141
#define __ULINUX_NR_reboot 142
#define __ULINUX_NR_setregid 143
#define __ULINUX_NR_setgid 144
#define __ULINUX_NR_setreuid 145
#define __ULINUX_NR_setuid 146
#define __ULINUX_NR_setresuid 147
#define __ULINUX_NR_getresuid 148
#define __ULINUX_NR_setresgid 149
#define __ULINUX_NR_getresgid 150
#define __ULINUX_NR_setfsuid 151
#define __ULINUX_NR_setfsgid 152
#define __ULINUX_NR_times 153
#define __ULINUX_NR_setpgid 154
#define __ULINUX_NR_getpgid 155
#define __ULINUX_NR_getsid 156
#define __ULINUX_NR_setsid 157
#define __ULINUX_NR_getgroups 158
#define __ULINUX_NR_setgroups 159
#define __ULINUX_NR_uname 160
#define __ULINUX_NR_sethostname 161
#define __ULINUX_NR_setdomainname 162

/* getrlimit and setrlimit are superseded with prlimit64 */
#define __ULINUX_NR_getrlimit 163
#define __ULINUX_NR_setrlimit 164

#define __ULINUX_NR_getrusage 165
#define __ULINUX_NR_umask 166
#define __ULINUX_NR_prctl 167
#define __ULINUX_NR_getcpu 168

/* kernel/time.c */
#define __ULINUX_NR_gettimeofday 169
#define __ULINUX_NR_settimeofday 170
#define __ULINUX_NR_adjtimex 171

/* kernel/timer.c */
#define __ULINUX_NR_getpid 172
#define __ULINUX_NR_getppid 173
#define __ULINUX_NR_getuid 174
#define __ULINUX_NR_geteuid 175
#define __ULINUX_NR_getgid 176
#define __ULINUX_NR_getegid 177
#define __ULINUX_NR_gettid 178
#define __ULINUX_NR_sysinfo 179

/* ipc/mqueue.c */
#define __ULINUX_NR_mq_open 180
#define __ULINUX_NR_mq_unlink 181
#define __ULINUX_NR_mq_timedsend 182
#define __ULINUX_NR_mq_timedreceive 183
#define __ULINUX_NR_mq_notify 184
#define __ULINUX_NR_mq_getsetattr 185

/* ipc/msg.c */
#define __ULINUX_NR_msgget 186
#define __ULINUX_NR_msgctl 187
#define __ULINUX_NR_msgrcv 188
#define __ULINUX_NR_msgsnd 189

/* ipc/sem.c */
#define __ULINUX_NR_semget 190
#define __ULINUX_NR_semctl 191
#define __ULINUX_NR_semtimedop 192
#define __ULINUX_NR_semop 193

/* ipc/shm.c */
#define __ULINUX_NR_shmget 194
#define __ULINUX_NR_shmctl 195
#define __ULINUX_NR_shmat 196
#define __ULINUX_NR_shmdt 197

/* net/socket.c */
#define __ULINUX_NR_socket 198
#define __ULINUX_NR_socketpair 199
#define __ULINUX_NR_bind 200
#define __ULINUX_NR_listen 201
#define __ULINUX_NR_accept 202
#define __ULINUX_NR_connect 203
#define __ULINUX_NR_getsockname 204
#define __ULINUX_NR_getpeername 205
#define __ULINUX_NR_sendto 206
#define __ULINUX_NR_recvfrom 207
#define __ULINUX_NR_setsockopt 208
#define __ULINUX_NR_getsockopt 209
#define __ULINUX_NR_shutdown 210
#define __ULINUX_NR_sendmsg 211
#define __ULINUX_NR_recvmsg 212

/* mm/filemap.c */
#define __ULINUX_NR_readahead 213

/* mm/nommu.c, also with MMU */
#define __ULINUX_NR_brk 214
#define __ULINUX_NR_munmap 215
#define __ULINUX_NR_mremap 216

/* security/keys/keyctl.c */
#define __ULINUX_NR_add_key 217
#define __ULINUX_NR_request_key 218
#define __ULINUX_NR_keyctl 219

/* arch/example/kernel/sys_example.c */
#define __ULINUX_NR_clone 220
#define __ULINUX_NR_execve 221

#define __ULINUX_NR_mmap 222
/* mm/fadvise.c */
#define __ULINUX_NR_fadvise64 223

/* mm/, CONFIG_MMU only */
#define __ULINUX_NR_swapon 224
#define __ULINUX_NR_swapoff 225
#define __ULINUX_NR_mprotect 226
#define __ULINUX_NR_msync 227
#define __ULINUX_NR_mlock 228
#define __ULINUX_NR_munlock 229
#define __ULINUX_NR_mlockall 230
#define __ULINUX_NR_munlockall 231
#define __ULINUX_NR_mincore 232
#define __ULINUX_NR_madvise 233
#define __ULINUX_NR_remap_file_pages 234
#define __ULINUX_NR_mbind 235
#define __ULINUX_NR_get_mempolicy 236
#define __ULINUX_NR_set_mempolicy 237
#define __ULINUX_NR_migrate_pages 238
#define __ULINUX_NR_move_pages 239

#define __ULINUX_NR_rt_tgsigqueueinfo 240
#define __ULINUX_NR_perf_event_open 241
#define __ULINUX_NR_accept4 242
#define __ULINUX_NR_recvmmsg 243

/*
 * Architectures may provide up to 16 syscalls of their own
 * starting with this value.
 */
#define __ULINUX_NR_arch_specific_syscall 244

#define __ULINUX_NR_wait4 260
#define __ULINUX_NR_prlimit64 261
#define __ULINUX_NR_fanotify_init 262
#define __ULINUX_NR_fanotify_mark 263
#define __ULINUX_NR_name_to_handle_at         264
#define __ULINUX_NR_open_by_handle_at         265
#define __ULINUX_NR_clock_adjtime 266
#define __ULINUX_NR_syncfs 267
#define __ULINUX_NR_setns 268
#define __ULINUX_NR_sendmmsg 269
#define __ULINUX_NR_process_vm_readv 270
#define __ULINUX_NR_process_vm_writev 271
#define __ULINUX_NR_kcmp 272
#define __ULINUX_NR_finit_module 273
#define __ULINUX_NR_sched_setattr 274
#define __ULINUX_NR_sched_getattr 275
#define __ULINUX_NR_renameat2 276
#define __ULINUX_NR_seccomp 277
#define __ULINUX_NR_getrandom 278
#define __ULINUX_NR_memfd_create 279
#define __ULINUX_NR_bpf 280
#define __ULINUX_NR_execveat 281
#define __ULINUX_NR_userfaultfd 282
#define __ULINUX_NR_membarrier 283
#define __ULINUX_NR_mlock2 284
#define __ULINUX_NR_copy_file_range 285
#define __ULINUX_NR_preadv2 286
#define __ULINUX_NR_pwritev2 287
#define __ULINUX_NR_pkey_mprotect 288
#define __ULINUX_NR_pkey_alloc 289
#define __ULINUX_NR_pkey_free 290
#define __ULINUX_NR_statx 291
#define __ULINUX_NR_io_pgetevents 292
#define __ULINUX_NR_rseq 293
#define __ULINUX_NR_kexec_file_load 294
/* 295 through 402 are unassigned to sync up with generic numbers, don't use */

#define __ULINUX_NR_pidfd_send_signal 424
#define __ULINUX_NR_io_uring_setup 425
#define __ULINUX_NR_io_uring_enter 426
#define __ULINUX_NR_io_uring_register 427
#define __ULINUX_NR_open_tree 428
#define __ULINUX_NR_move_mount 429
#define __ULINUX_NR_fsopen 430
#define __ULINUX_NR_fsconfig 431
#define __ULINUX_NR_fsmount 432
#define __ULINUX_NR_fspick 433
/*---------------------------------------------------------------------------*/
/* the following is a mapping to get the same on x86_64 and aarch64 */
#define __ULINUX_NR_newfstatat __ULINUX_NR_fstatat 
#endif	/* ULINUX_ARCH_SYSC_H */
