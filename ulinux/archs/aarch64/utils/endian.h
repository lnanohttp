#ifndef ULINUX_ARCH_UTILS_ENDIAN_H
#define ULINUX_ARCH_UTILS_ENDIAN_H
/*
 * this code is protected by the GNU affero GPLv3
 * author:Sylvain BERTRAND
 */

#ifdef __GNUC__
static inline ulinux_u16 ulinux_cpu_to_be16(ulinux_u16 v)
{
	return __builtin_bswap16(v);
}
#define ulinux_be16_to_cpu(v) ulinux_cpu_to_be16(v)

static inline ulinux_u32 ulinux_cpu_to_be32(ulinux_u32 v)
{
	return __builtin_bswap32(v);
}
#define ulinux_be32_to_cpu(v) ulinux_cpu_to_be32(v)

static inline ulinux_u64 ulinux_cpu_to_be64(ulinux_u64 v)
{
	return __builtin_bswap64(v);
}
#define ulinux_be64_to_cpu(v) ulinux_cpu_to_be64(v)
#else
#error "missing endian conversions for your toolchain"
#endif

/* little endian */
#define ulinux_cpu_to_le32(v) (v)
#define ulinux_cpu_to_le64(v) (v)
#define ulinux_le32_to_cpu(v) (v)
#define ulinux_le64_to_cpu(v) (v)
#endif
