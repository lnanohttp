#ifndef ULINUX_UTILS_ASCII_BLOCK_CONV_DECIMAL_DECIMAL_C
#define ULINUX_UTILS_ASCII_BLOCK_CONV_DECIMAL_DECIMAL_C
/*
 * this code is protected by the GNU affero GPLv3
 * author:Sylvain BERTRAND
 */
#include <stdbool.h>

#include <ulinux/compiler_types.h>
#include <ulinux/types.h>

#include <ulinux/utils/ascii/ascii.h>

/*----------------------------------------------------------------------------*/
/* "One Compilation Unit" support */
#ifdef ULINUX_UTILS_EXTERNAL
#define ULINUX_EXPORT
#else
#define ULINUX_EXPORT static
#endif
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/* local */
#define loop for(;;)
#define CSTRLEN(cstr) (sizeof(cstr) - 1)
/*----------------------------------------------------------------------------*/

/*
 * strict unsigned decimal ascii block to byte
 * caller must provide a valid memory block
 * inplace conversion: ok
 */
ULINUX_EXPORT bool ulinux_dec2u8_blk(ulinux_u8 *dest, ulinux_u8 *start,
								ulinux_u8 *last)
{/* do *not* trust content */
	ulinux_u16 buf;

	if ((ulinux_u64)(last - start) >= CSTRLEN("255"))
		return false;
	buf = 0;
	loop {
		if (start > last)
			break;

		if (!ulinux_is_digit(*start))
			return false;
		buf = buf * 10 + (*start - '0');
		++start;
	}
	/* overflow, max is 999 way below what can do a u16 */
	if (buf & 0xff00)
		return false;
	*dest = (ulinux_u8)buf;
	return true;
}

/*
 * strict unsigned decimal ascii block to u16
 * caller must provide a valid memory block
 * inplace conversion: ok
 */
ULINUX_EXPORT bool ulinux_dec2u16_blk(ulinux_u16 *dest, ulinux_u8 *start,
								ulinux_u8 *last)
{/* do *not* trust content */
	ulinux_u32 buf;

	if ((ulinux_u64)(last - start) >= CSTRLEN("65535"))
		return false;
	buf = 0;
	loop {
		if (start > last)
			break;

		if (!ulinux_is_digit(*start))
			return 0;
		buf = buf * 10 + (*start - '0');
		++start;
	}
	/* overflow, max is 99999 way below what can do a u32 */
	if (buf & 0xffff0000)
		return false;
	*dest = (ulinux_u16)buf;
	return true;
}

/*
 * strict unsigned decimal ascii block to u32
 * caller must provide a valid memory block
 * inplace conversion: ok
 */
ULINUX_EXPORT bool ulinux_dec2u32_blk(ulinux_u32 *dest, ulinux_u8 *start,
								ulinux_u8 *last)
{/* do *not* trust content */
	ulinux_u64 buf;

	if ((ulinux_u64)(last - start) >= CSTRLEN("4294967295"))
		return false;
	buf = 0;
	loop {
		if (start > last)
			break;

		if (!ulinux_is_digit(*start))
			return false;
		buf = buf * 10 + (*start - '0');
		++start;
	}
	/* overflow, max is 9999999999 way below what can do a u64 */
	if (buf & 0xffffffff00000000)
		return false;
	*dest = (ulinux_u32)buf;
	return true;
}

/*
 * strict unisgned decimal ascii block to u64
 * caller must provide a valid memory block
 * inplace conversion: ok
 */
ULINUX_EXPORT bool ulinux_dec2u64_blk(ulinux_u64 *dest, ulinux_u8 *start,
								ulinux_u8 *last)
{/* do *not* trust */
	ulinux_u64 buf;

	if ((ulinux_u64)(last - start) >= CSTRLEN("18446744073709551615"))
		return false;
	buf = 0;
	loop {
		ulinux_u64 old_buf;

		if(start > last)
			break;

		if(!ulinux_is_digit(*start))
			return false;

		old_buf = buf;
		buf = buf * 10 + (*start - '0');
		if (buf < old_buf) /* overflow check */
			return false;
		++start;
	}
	*dest = buf;
	return true;
}

/*----------------------------------------------------------------------------*/
/* local cleanup*/
#undef loop
#undef CSTRLEN
/*----------------------------------------------------------------------------*/
#undef ULINUX_EXPORT
#endif
