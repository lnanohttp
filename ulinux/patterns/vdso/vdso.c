/* see the start pattern */
/*
 * COMMON ABBREVIATIONS:
 * dyn : dynamic
 * ehdr : Extended HeaDeR
 * hdr : HeaDeR
 * idx(s) : InDeX(S)
 * phdr(s) : Program segment HeaDeR(S)
 * ptr(s) : PoinTeR(S)
 * seg(s): program SEGment(S)
 * spec(s) : specification(s)
 * tab(s) : TABLe(S)
 * val(s) : VALue(S)
 * rvaddr : Real Virual ADDRess
 */
#include <stdarg.h>
#include <stdbool.h>

#include <ulinux/compiler_types.h>
#include <ulinux/types.h>
#include <ulinux/sysc.h>

#include <ulinux/compiler_misc.h>

#include <ulinux/error.h>

#include <ulinux/start.h>
#include <ulinux/elf.h>

#include <ulinux/utils/mem.h>
#include <ulinux/utils/ascii/string/string.h>
#include <ulinux/utils/ascii/string/vsprintf.h>

#include <ulinux/file.h>
#include <ulinux/time.h>

#define ui ulinux_ui
#define si ulinux_si
#define ul ulinux_ul
#define sl ulinux_sl
#define u8 ulinux_u8
#define u16 ulinux_u16
#define u32 ulinux_u32
#define u64 ulinux_u64
/* kill process, aka thread group */
#define exit(code) ulinux_sysc(exit_group,1,code)
#define elf64_auxv ulinux_elf64_auxv
#define AT_NULL ULINUX_AT_NULL
#define AT_SYSINFO_EHDR ULINUX_AT_SYSINFO_EHDR
#define strcmp(a,b) ulinux_strcmp((u8*)a,(u8*)b)
#define ISERR ULINUX_ISERR
#define CLOCK_MONOTONIC_RAW ULINUX_CLOCK_MONOTONIC_RAW
#define timespec ulinux_timespec
/* elf tabs */
#define PT_LOAD ULINUX_PT_LOAD
#define PT_DYNAMIC ULINUX_PT_DYNAMIC
#define elf64_ehdr ulinux_elf64_ehdr
#define elf64_phdr ulinux_elf64_phdr
#define DT_NULL ULINUX_DT_NULL
#define DT_HASH ULINUX_DT_HASH
#define DT_STRTAB ULINUX_DT_STRTAB
#define DT_SYMTAB ULINUX_DT_SYMTAB
#define DT_VERSYM ULINUX_DT_VERSYM
#define DT_VERDEF ULINUX_DT_VERDEF
#define elf64_dyn ulinux_elf64_dyn
#define STN_UNDEF ULINUX_STN_UNDEF
#define elf64_sym ulinux_elf64_sym

/* convenience macros */
#define BUFSIZ 8192
static u8 dprint_buf[BUFSIZ];
#define POUT(fmt,...) ulinux_dprintf(1,&dprint_buf[0],BUFSIZ-1,fmt,##__VA_ARGS__)

#define loop for(;;)

#if BITS_PER_LONG != 64
#error "sorry, only elf64 code"
#endif

/*----------------------------------------------------------------------------*/
/* vdso getcpu */
struct getcpu_cache {/* dummy */};
static sl (*getcpu)(ui *cpu, ui *node, struct getcpu_cache *tcache);
#define VDSO_GETCPU_CAST(x) (sl(*)(ui *cpu, ui *node, struct getcpu_cache *tcache))(x)
#define VDSO_GETCPU_SYM "__vdso_getcpu"
/*----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------*/
/* vdso clock_gettime */
static sl (*clock_gettime)(si clock_id, struct timespec *timespec);
#define VDSO_CLOCK_GETTIME_CAST(x) (sl (*)(si clock_id, struct timespec *timespec))(x)
#define VDSO_CLOCK_GETTIME_SYM "__vdso_clock_gettime"
/*----------------------------------------------------------------------------*/


/*============================================================================*/
static u8 *vdso;
static u64 vdso_load_offset;
/*----------------------------------------------------------------------------*/
/* the following are the "dynamic" version elf sections */
static u32 *vdso_hash; 		/* hash hdr is 2 32 bits words */
static u32 *vdso_hash_buckets;	/* 32 bits words, each one is a chain idx */
static u32 vdso_hash_buckets_n;	/* first 32 bits word of the hdr */
static u32 *vdso_hash_chains;	/* 32 bit words, each one is a symtab idxs */
static u32 vdso_hash_chains_n;	/* second 32 bits word of the hdr */
/*----------------------------------------------------------------------------*/
/* the following are the "dynamic" version elf sections */
static u8 *vdso_strtab;
static struct elf64_sym *vdso_symtab;
static u8 *vdso_versym;
static u8 *vdso_verdef;
/*----------------------------------------------------------------------------*/

static void vdso_sym_select(u32 idx)
{
	if (strcmp(vdso_strtab + vdso_symtab[idx].name, VDSO_GETCPU_SYM) == 0) {
		getcpu = VDSO_GETCPU_CAST(vdso_symtab[idx].value
							+ vdso_load_offset);
		POUT("getcpu found at 0x%p\n", getcpu);
		return;
	}

	if (strcmp(vdso_strtab + vdso_symtab[idx].name, VDSO_CLOCK_GETTIME_SYM)
									== 0) {
		clock_gettime = VDSO_CLOCK_GETTIME_CAST(vdso_symtab[idx].value
							+ vdso_load_offset);
		POUT("clock_gettime found at 0x%p\n", clock_gettime);
		return;
	}
}

static void vdso_hash_scan(void)
{
	u32 bucket;
	u32 chain;

	bucket = 0;
	chain = STN_UNDEF;
	loop {
		u32 chain;

		if (bucket == vdso_hash_buckets_n)
			break;

		chain = vdso_hash_buckets[bucket];
		loop {
			if (chain == STN_UNDEF)
				break;
			vdso_sym_select(chain);

			/* next in chain */
			chain = vdso_hash_chains[chain];
		}

		++bucket;
	}
}

static void vdso_dyns(struct elf64_dyn *dyn)
{
	loop {
		if (dyn->tag == DT_NULL) {
			break;
		} else if (dyn->tag == DT_STRTAB) {
			vdso_strtab = (u8*)(dyn->ptr + vdso_load_offset);
		} else if (dyn->tag == DT_SYMTAB) {
			vdso_symtab = (struct elf64_sym*)(dyn->ptr
							+ vdso_load_offset);
		} else if (dyn->tag == DT_HASH) {
			vdso_hash = (u32*)(dyn->ptr + vdso_load_offset);
			vdso_hash_buckets_n = vdso_hash[0];
			vdso_hash_chains_n = vdso_hash[1];
			vdso_hash_buckets = &vdso_hash[2];
			vdso_hash_chains =
					&vdso_hash_buckets[vdso_hash_buckets_n];
		}
		++dyn;
	}
	vdso_hash_scan();
}

static void vdso_auxv(void)
{
	struct elf64_ehdr *ehdr;
	struct elf64_phdr *phdrs;
	u16 i;
	bool load_seg_found;
	bool dyn_seg_found;
	u64 dyns;

	ehdr = (struct elf64_ehdr*)vdso;
	phdrs = (struct elf64_phdr*)(vdso + ehdr->phoff);

	load_seg_found = false;
	dyn_seg_found = false;
	i = 0;
	loop {
		if (i == ehdr->phnum)
			break;

		if ((phdrs[i].type == PT_LOAD) && !load_seg_found) {
			load_seg_found = true;
			vdso_load_offset = (u64)vdso - phdrs[i].vaddr;
			
		} else if (phdrs[i].type == PT_DYNAMIC) { 
			dyn_seg_found = true;

			dyns = phdrs[i].vaddr;
		}
		++i;
	}

	if (!load_seg_found) {
		POUT("not load elf program segment found\n");
		return;
	}

	if (!dyn_seg_found) {
		POUT("not dynamic elf program segment, hence dynamic elf section, found\n");
		return;
	}

	/* XXX: compute the rvaddr with the offset we now have */
	dyns += vdso_load_offset;
	vdso_dyns((struct elf64_dyn*)dyns);
}
/* vdso */
/*============================================================================*/

/* shows what arch ABIs, linux ABI, put on the initial stack */
void ulinux_start(u8 *stack) 
{
	ul *argc;
	u8 **argv;
	u8 **envp;
	struct elf64_auxv *auxv;
	ul arg_idx;
	
	dprint_buf[BUFSIZ - 1] = 0; /* secure a 0 terminating char */
	getcpu = 0;

	/*--------------------------------------------------------------------*/

	/* skip argc */
	argc = (ul*)stack;
	stack += sizeof(*argc);

	/*--------------------------------------------------------------------*/

	/* skik argv, the slow way */
	argv = (u8**)stack;
	arg_idx = 0;
	loop {
		if (*argv == 0)
			break;
		++argv;
		++arg_idx;
	}

	/* skip ending 0 pointer */
	++argv;
	stack = (u8*)argv;

	/*--------------------------------------------------------------------*/

	/* skip envp */
	envp = (u8**)stack;
	loop {
		if (*envp == 0)
			break;
		++envp;
	}

	/* skip ending 0 pointer */
	++envp;
	stack = (u8*)envp;

	/*--------------------------------------------------------------------*/

	/* parse auxv */
	auxv = (struct elf64_auxv*)stack;
	loop {
		if (auxv->type == AT_NULL)
			break;
		if (auxv->type == AT_SYSINFO_EHDR) {
			vdso = (u8*)auxv->val;
		}
		++auxv;
	}

	/* vdso auxv, linux ABI specific */
	vdso_auxv();

	/*====================================================================*/

	/* high performance vdso calls */
	
	if (getcpu != 0) {
		sl r;
		ui cpu;
		ui node;

		r = getcpu(&cpu, &node, 0);
		if (ISERR(r))
			POUT("error while calling vdso getcpu\n");
		else
			POUT("getcpu:cpu=%u;node=%u\n", cpu, node);
	}

	if (clock_gettime != 0) {
		sl r;
		struct timespec timespec;

		r = clock_gettime(CLOCK_MONOTONIC_RAW, &timespec);
		if (ISERR(r))
			POUT("error while calling vdso clock_gettime\n");
		else
			POUT("clock_gettime:sec=%lu:nsec=%lu\n", timespec.sec, timespec.nsec);
	}
	exit(0);
}
