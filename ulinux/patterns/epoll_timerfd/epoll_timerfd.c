/*
 * XXX: if you are a heavy user of time related calls, you should use vdso
 * calls instead of syscalls. That depends on the arch.
 */
#include <stdarg.h>

#include <ulinux/compiler_types.h>
#include <ulinux/compiler_misc.h>
#include <ulinux/sysc.h>
#include <ulinux/types.h>
#include <ulinux/epoll.h>
#include <ulinux/file.h>
#include <ulinux/error.h>
#include <ulinux/time.h>

#include <ulinux/utils/mem.h>
#include <ulinux/utils/ascii/string/string.h>
#include <ulinux/utils/ascii/string/vsprintf.h>

/* ulinux namespace */
#define EINTR ULINUX_EINTR
#define EAGAIN ULINUX_EAGAIN
#define si ulinux_si
#define sl ulinux_sl
#define u8 ulinux_u8
#define u64 ulinux_u64
/* kill process, aka thread group */
#define exit(code) ulinux_sysc(exit_group,1,code)
#define ISERR ULINUX_ISERR
#define CLOCK_MONOTONIC ULINUX_CLOCK_MONOTONIC
#define clock_gettime(a,b) ulinux_sysc(clock_gettime,2,a,b)
#define TFD_NONBLOCK ULINUX_TFD_NONBLOCK
#define TFD_TIMER_ABSTIME ULINUX_TFD_TIMER_ABSTIME
#define itimerspec ulinux_itimerspec
#define timerfd_create(a,b) ulinux_sysc(timerfd_create,2,a,b)
#define timerfd_settime(a,b,c,d) ulinux_sysc(timerfd_settime,4,a,b,c,d)
#define epoll_create1(a) ulinux_sysc(epoll_create1,1,a)
#define epoll_event ulinux_epoll_event
#define memset(a,b,c) ulinux_memset((ulinux_u8*)a,b,c)
#define EPOLLIN ULINUX_EPOLLIN
#define epoll_ctl(a,b,c,d) ulinux_sysc(epoll_ctl,4,a,b,c,d)
#define EPOLL_CTL_ADD ULINUX_EPOLL_CTL_ADD
/* common to x86_64 and aarch64 */
#define epoll_pwait(a,b,c,d,e) ulinux_sysc(epoll_pwait,5,a,b,c,d,e)
#define read(a,b,c) ulinux_sysc(read,3,a,b,c)

/* convenience macros */
#define BUFSIZ 8192
static u8 dprint_buf[BUFSIZ];
#define POUT(fmt,...) ulinux_dprintf(1,&dprint_buf[0],BUFSIZ-1,fmt,##__VA_ARGS__)
#define EPOLL_EVENTS_N 10
#define loop for(;;)

#define INITIAL_EXPIRATION_SECS 8

void _start(void)
{
	sl r;
	si timerfd;
	struct itimerspec itimerspec;
	si epfd;
	struct epoll_event evts[EPOLL_EVENTS_N];
	sl evt;

	dprint_buf[BUFSIZ - 1] = 0; /* secure a 0 terminating char */

	timerfd = (si)timerfd_create(CLOCK_MONOTONIC, TFD_NONBLOCK);
	if (ISERR(timerfd)) {
		POUT("unable to create the timer fd (%d)\n", timerfd);
		exit(1);
	}

	memset(&itimerspec, 0, sizeof(itimerspec));

	/*--------------------------------------------------------------------*/

	r = clock_gettime(CLOCK_MONOTONIC, &itimerspec.value);
	if (ISERR(r)) {
		POUT("error while getting the current monotonic clock time (%ld)\n", r);
		exit(2);
	}
	POUT("current monotonic clock is %ld secs and %ld nsecs\n", itimerspec.value.sec, itimerspec.value.nsec);

	/*--------------------------------------------------------------------*/

	/* initial expiration in the futur of current monotonic clock */
	itimerspec.value.sec += INITIAL_EXPIRATION_SECS;
	itimerspec.value.nsec = 0;

	/* we are looking for an absolute initial expiration */
	r = timerfd_settime(timerfd, TFD_TIMER_ABSTIME, &itimerspec, 0);
	if (ISERR(r)) {
		POUT("unable to arm the timer\n");
		exit(3);
	}

	epfd = (si)epoll_create1(0);
	if (ISERR(epfd)) {
		POUT("unable to create the epoll fd\n");
		exit(4);
	}

	memset(evts, 0, sizeof(evts));
	evts[0].events = EPOLLIN; /* could be EPOLLET too */
	evts[0].data.fd = timerfd;
	r = epoll_ctl(epfd, EPOLL_CTL_ADD, timerfd, &evts[0]);
	if (ISERR(r)) {
		POUT("unable to add the timer fd to the epoll fd (%ld)\n", r);
		exit(5);
	}

	loop {		
		memset(evts, 0, sizeof(evts));
		r = epoll_pwait(epfd, evts, EPOLL_EVENTS_N, -1, 0);
		if (r != -EINTR)
			break;

		POUT("epoll_pwait was interruped by a signal (we did not set any timeout), restarting\n");
	}
	if (ISERR(r)) {
		POUT("epoll_wait error (%ld)\n", r);
		exit(6);
	}

	evt = 0;
	loop {
		if (evt == r)
			break;

		if (evts[evt].data.fd == timerfd) {
			if ((evts[evt].events & EPOLLIN) != 0) {
				u64 expirations_n = 0; /* the count of expirations of our timer */

				loop {/* reads are atomic or err, aka no short reads */
					r = read(timerfd, &expirations_n, sizeof(u64));
					if (r != -EINTR);
						break;
				}

				if (r == -EAGAIN) {  /* for a non blocking fd, means we read it */
					POUT("something is wrong: we got notified of some timer expirations, but there is no count of expirations!\n");
					exit(7);
				}
				if (ISERR(r)) {
					POUT("something went wrong while reading the count of expirations (%ld)\n", r);
					exit(8);
				}
				POUT("count of expirations=%lu\n", expirations_n);
			} else {
				POUT("got an unwanted event on the timer fd\n");
				exit(9);
			}
		}
		++evt; /* next epoll event */
	}

	/*--------------------------------------------------------------------*/

	memset(&itimerspec, 0, sizeof(itimerspec));
	r = clock_gettime(CLOCK_MONOTONIC, &itimerspec.value);
	if (ISERR(r)) {
		POUT("error while getting the current monotonic clock time at exit time(%ld)\n", r);
		exit(10);
	}
	POUT("monotonic clock at exit is %ld secs and %ld nsecs\n", itimerspec.value.sec, itimerspec.value.nsec);
	exit(0);
}
